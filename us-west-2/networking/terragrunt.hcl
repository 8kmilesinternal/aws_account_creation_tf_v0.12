# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "../../../aws_modules_tf_v0.12/networking"
  #source = "git::ssh://git@altssh.bitbucket.org:443/8kmilesinternal/aws_modules_tf_v0.12.git//networking"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

# dependency "governance" {
#  config_path = "../reghub_tgw_share"
# }

dependency "iam" {
  config_path = "../../global/iam"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {

  # DHCP
  enable_dhcp_options               = true
  dhcp_options_domain_name          = "science.roche.com"
  dhcp_options_domain_name_servers  = ["10.158.10.113","10.158.10.39", "10.158.10.139"]
  dhcp_options_ntp_servers          = ["10.158.10.39", "10.158.10.139"]
  dhcp_options_netbios_name_servers = ["10.158.10.39", "10.158.10.139"]
  dhcp_options_netbios_node_type    = "8"

  vpcflowlog_role_arn = dependency.iam.outputs.vpcflowlog_role_arn

}
