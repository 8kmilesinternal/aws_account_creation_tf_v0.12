# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  #source = "git::ssh://git@altssh.bitbucket.org:443/8kmilesinternal/aws_modules_tf_v0.12.git//autotag"
  source = "../../../aws_modules_tf_v0.12/autotag_cloudwatch"
}
# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

dependency "autotag" {
  config_path = "../../global/autotag"
}
dependency "ec2amicheck" {
  config_path = "../../global/ec2amicheck"
}
# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {
  lambda_autotag  = dependency.autotag.outputs.lambda_autotag
  #lambda_ec2amicheck  = dependency.ec2amicheck.outputs.lambda_ec2amicheck
  # lambda_role_arn = dependency.iam.outputs.lambda_autotag_role
}
