
# Configure Terragrunt to automatically store tfstate files in an S3 bucket
remote_state {
  backend = "s3"
  config = {
    encrypt        = true
    #bucket         = "test-dataanlystics-1010" #testtfstate1010
    bucket         = get_env("TF_STATE_BUCKETNAME", "") 
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "test-terraform-locks"
  }
}
# Configure root level variables that all resources can inherit. This is especially helpful with multi-account configs
# where terraform_remote_state data sources are placed directly into the modules.

inputs = merge(
  # Configure Terragrunt to use common vars encoded as yaml to help you keep often-repeated variables (e.g., account ID)
  # DRY. We use yamldecode to merge the maps into the inputs, as opposed to using varfiles due to a restriction in
  # Terraform >=0.12 that all vars must be defined as variable blocks in modules. Terragrunt inputs are not affected by
  # this restriction.
  yamldecode(
    file("${get_terragrunt_dir()}/${find_in_parent_folders("account.yaml")}"),
  ),
  yamldecode(
    #file("${get_terragrunt_dir()}/${find_in_parent_folders("region.yaml")}"),
    file("${get_terragrunt_dir()}/${find_in_parent_folders("region.yaml", "${find_in_parent_folders("account.yaml")}")}"),
  ),
  {
    account_id = "${get_aws_account_id()}"
    cloudtrail_bucket = "test-ctr-bucket-manag",
    config_bucket = "test-config-bucket-manag",
    aws_region = "us-east-1"
  },
)

