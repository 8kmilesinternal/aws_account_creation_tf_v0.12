# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  #source = "git::ssh://git@altssh.bitbucket.org:443/8kmilesinternal/aws_modules_tf_v0.12.git//cloudwatch"
  source = "../../../aws_modules_tf_v0.12/cloudwatch"
}
# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
dependency "cloudtrail" {
  config_path = "../cloudtrail"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {
  #aws_region                   = "us-east-1"
  cloudtrail_loggroup = dependency.cloudtrail.outputs.ct_loggroup
}