# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  #source = "git::ssh://git@altssh.bitbucket.org:443/8kmilesinternal/aws_modules_tf_v0.12.git//accesskeyage"
  source = "../../../aws_modules_tf_v0.12/accesskeyage"
}
# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

dependency "iam" {
  config_path = "../../global/iam"
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {
  #aws_region                   = "us-west-2"
  lambda_role = dependency.iam.outputs.lambda_role
  notifyops_snstopic = dependency.iam.outputs.notifyops_snstopic
}
